// console.log("Hello");

let trainer = {
	name: "Brock",
	age: 12,
	pokemon:["Squirtle", "Butterfree", "Meowth"],
	friends:{
		Hoenn: ["Misty", "Brock"],
		kanto: ["May", "Max"]
	},

	talk: function(){
		console.log("Pikachu! I choose you!");
	}

}

console.log(trainer);

console.log("Result of the dot notation");
console.log(trainer.name);

console.log("Result of the square bracket notation");
console.log(trainer.pokemon);

console.log("Result of the Talk Method");
trainer.talk();

function Pokemon(name,level,health, attack){
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;

	this.tackle = function(attackedPokemon){
		console.log(this.name + " tackled " + attackedPokemon)
	}

	this.faint = function(){
		console.log(this.name + " has fainted");
	}
}

let pokemon1 = new Pokemon("Blastoise",15,);
console.log(pokemon1);

let pokemon2 = new Pokemon("Caterpie",10);
console.log(pokemon2);

let pokemon3 = new Pokemon("Blastoise",20);
console.log(pokemon3);

pokemon2.tackle(pokemon1.name);
pokemon3.tackle(pokemon2.name);
pokemon3.faint();
